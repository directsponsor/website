
This is the main directsponsor.org website. The accounts section is a separate installation (listed under the main [direct sponsor bitbucket ](https://bitbucket.org/directsponsor/) account). The main site is running a standard wordpress installation, with customizations and the whole system is intended to be open sourced when finished.

The Direct Sponsor system doesn't handle money at all; sponsors send their money directly to the recipients, thus we are happy to receive voluntary help with coding or visual design.